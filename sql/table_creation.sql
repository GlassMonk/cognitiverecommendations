CREATE DATABASE IF NOT EXISTS cognitive_recommendations;

USE cognitive_recommendations;

DROP TABLE IF EXISTS FragranceRating;
DROP TABLE IF EXISTS EffectivenessRating;
DROP TABLE IF EXISTS Product;
DROP TABLE IF EXISTS FragranceRecommendation;
DROP TABLE IF EXISTS EffectivenessRecommendation;

CREATE TABLE IF NOT EXISTS Product
(
  id varchar(255),
  title varchar(255),
  category varchar(255),
  fragranceFamily varchar(255),
  chemicalFamily varchar(255),
  fragranceRating float,
  PRIMARY KEY (ID)
);

CREATE TABLE  IF NOT EXISTS FragranceRating
(
  userId varchar(255),
  productId varchar(255),
  rating int,
  PRIMARY KEY(productId, userId),
  FOREIGN KEY (productId) 
    REFERENCES Product(id)
);

CREATE TABLE  IF NOT EXISTS EffectivenessRating
(
  userId varchar(255),
  productId varchar(255),
  rating int,
  PRIMARY KEY(productId, userId),
  FOREIGN KEY (productId) 
    REFERENCES Product(id)
);

CREATE TABLE  IF NOT EXISTS FragranceRecommendation
(
  userId varchar(255),
  productId varchar(255),
  prediction float,
  PRIMARY KEY(userId, productId),
  FOREIGN KEY (productId) 
    REFERENCES Product(id)
);

CREATE TABLE  IF NOT EXISTS EffectivenessRecommendation
(
  userId varchar(255),
  productId varchar(255),
  prediction float,
  PRIMARY KEY(userId, productId),
  FOREIGN KEY (productId) 
    REFERENCES Product(id)
);



