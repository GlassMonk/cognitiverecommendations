# README #

### SOLUTION OVERVIEW ###

#### GOAL ####
The goal here is to predict the fragrance family that is likely to be the most appealing to a given consumer, and the chemical base that is likely to be the most effective for them.

#### INPUTS ####
* Product data
* Fragrance Ratings data and Effectiveness Ratings data from consumers

#### OUTPUTS ####
* The simple prototype predicts two things:
    * The top five fragrance families that a consumer is most likely to like
    * The top five chemical bases that the consumer is most likely to find effective

#### SOLUTION APPROACH AND MODEL CONSIDERATIONS ####
* Given the use case, the solution uses a �collaborative� filtering approach under the premise that two users who liked the same products in the past will probably continue to like the same new ones.
* The solution uses a model-based approach to filtering where users have rated the products.
* The code implements a Spark MLlib ALS algorithm to train the models.
* The dataset is broken into 60%:20%:20% for training, validating and testing.
* The final predictions are saved in a MySQL table.

#### CODE ####
* This code has been adapted from Google Cloud Platform's spark-recommendation-engine project that outlines a recommendation model for Accommodations.
* The code has been adapted and updated to train and execute two models based on Product data
* The original source is available @ https://github.com/GoogleCloudPlatform/spark-recommendation-engine/tree/master/pyspark.

#### DESCRIPTION ####
* model_generator.py is the file that is used to train the model.
* model_executor.py is the file that is used to execute the model.
* Database creation script and training data are available in the sql folder

#### EXECUTION ####
* The easiest way to execute the code is to run it on the Google Cloud Platform.
    * Setup a Spark cluster using Cloud Dataproc.
    * Create a Cloud SQL database using the table_creation.sql.
    * Import the products data and the ratings data included in the .csv files.
    * Run model_generator.py on the Spark cluster to find the best fragrance and effectiveness models. The output of this will be the optimal Rank, Regul and Iter values for each of the models.
    * Run model_executor.py to make a prediction using the optimal Rank, Regul and Iter values and persist them in the Cloud SQL database.
    * Query the Recommendation tables to view the predictions
