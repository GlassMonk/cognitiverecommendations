#!/usr/bin/env python
"""
This code has been adapted from Google Cloud Platform's spark-recommendation-engine project that outlines a
recommendation model for Accommodations.
The code has been adapted and updated to train two models based on Product data
The original source is available @ https://github.com/GoogleCloudPlatform/spark-recommendation-engine/tree/master/pyspark
"""
from __future__ import print_function

import sys
import itertools
from math import sqrt
from operator import add
from os.path import join, isfile, dirname
from pyspark import SparkContext, SparkConf, SQLContext
from pyspark.mllib.recommendation import ALS, MatrixFactorizationModel, Rating

ranks  = [5,10,15,20]
reguls = [0.1, 1,10]
iters  = [5,10,20]

#Test parameters
#CLOUDSQL_INSTANCE_IP = "35.196.146.149"
#CLOUDSQL_DB_NAME = "cognitive_recommendations"
#CLOUDSQL_USER = "appuser"
#CLOUDSQL_PWD  = "n0r00t4u"

CLOUDSQL_INSTANCE_IP = sys.argv[1]
CLOUDSQL_DB_NAME = sys.argv[2]
CLOUDSQL_USER = sys.argv[3]
CLOUDSQL_PWD  = sys.argv[4]

conf = SparkConf().setAppName("app_cognitiverecommendations")
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

jdbcUrl    = 'jdbc:mysql://%s:3306/%s?user=%s&password=%s&useSSL=false' % (CLOUDSQL_INSTANCE_IP, CLOUDSQL_DB_NAME, CLOUDSQL_USER, CLOUDSQL_PWD)

#[START how_far]
def howFarAreWe(model, against, sizeAgainst):
    # Ignore the rating column  
    againstNoRatings = against.map(lambda x: (int(x[0]), int(x[1])) )

    # Keep the rating to compare against
    againstWiRatings = against.map(lambda x: ((int(x[0]),int(x[1])), int(x[2])) )

    # Make a prediction and map it for later comparison
    # The map has to be ((user,product), rating) not ((product,user), rating)
    predictions = model.predictAll(againstNoRatings).map(lambda p: ( (p[0],p[1]), p[2]) )

    # Returns the pairs (prediction, rating)
    predictionsAndRatings = predictions.join(againstWiRatings).values()

    # Returns the variance
    return sqrt(predictionsAndRatings.map(lambda s: (s[0] - s[1]) ** 2).reduce(add) / float(sizeAgainst))
#[END how_far]



# Read the data from the Cloud SQL
# Create dataframes-fragranceRating
dfRatingsFragrance = sqlContext.read.jdbc(url=jdbcUrl, table='FragranceRating')

rddUserRatingsFragrance = dfRatingsFragrance.filter(dfRatingsFragrance.userId == 0).rdd
print(rddUserRatingsFragrance.count())

# Split the data in 3 different sets : training, validating, testing
# 60% 20% 20%
rddRatingsFragrance = dfRatingsFragrance.rdd
rddTrainingFragrance, rddValidatingFragrance, rddTestingFragrance = rddRatingsFragrance.randomSplit([6,2,2])

#Add user ratings in the training model
rddTrainingFragrance.union(rddUserRatingsFragrance)
nbValidatingFragrance = rddValidatingFragrance.count()
nbTestingFragrance    = rddTestingFragrance.count()

print("Training: %d, validation: %d, test: %d" % (rddTrainingFragrance.count(), nbValidatingFragrance, rddTestingFragrance.count()))

finalModelFragrance = None
finalRankFragrance  = 0
finalRegulFragrance = float(0)
finalIterFragrance  = -1
finalDistFragrance   = float(100)

#[START train_model_fragrance]
for cRank, cRegul, cIter in itertools.product(ranks, reguls, iters):
    
    modelFragrance = ALS.train(rddTrainingFragrance, cRank, cIter, float(cRegul))
    dist = howFarAreWe(modelFragrance, rddValidatingFragrance, nbValidatingFragrance)
    if dist < finalDistFragrance:
        print("Best so far:%f" % dist)
        finalModelFragrance = modelFragrance
        finalRankFragrance  = cRank
        finalRegulFragrance = cRegul
        finalIterFragrance  = cIter
        finalDistFragrance  = dist
#[END train_model_fragrance]]

print() 
print("Fragrance Rank %i" % finalRankFragrance) 
print("Fragrance Iter %i" % finalIterFragrance)  
print("Fragrance Regul %f" % finalRegulFragrance)
print("Fragrance Dist %f" % finalDistFragrance) 

# Read the data from the Cloud SQL
# Create dataframes-effectivenessRating
dfRatingsEffectiveness = sqlContext.read.jdbc(url=jdbcUrl, table='EffectivenessRating')

rddUserRatingsEffectiveness = dfRatingsEffectiveness.filter(dfRatingsEffectiveness.userId == 0).rdd
print(rddUserRatingsEffectiveness.count())

# Split the data in 3 different sets : training, validating, testing
# 60% 20% 20%
rddRatingsEffectiveness = dfRatingsEffectiveness.rdd
rddTrainingEffectiveness, rddValidatingEffectiveness, rddTestingEffectiveness = rddRatingsEffectiveness.randomSplit([6,2,2])

#Add user ratings in the training model
rddTrainingEffectiveness.union(rddUserRatingsEffectiveness)
nbValidatingEffectiveness = rddValidatingEffectiveness.count()
nbTestingEffectiveness    = rddTestingEffectiveness.count()

print("Training: %d, validation: %d, test: %d" % (rddTrainingEffectiveness.count(), nbValidatingEffectiveness, rddTestingEffectiveness.count()))

inalModelEffectiveness = None
finalRankEffectiveness  = 0
finalRegulEffectiveness = float(0)
finalIterEffectiveness  = -1
finalDistEffectiveness   = float(100)

#[START train_model_effectiveness]
for cRank, cRegul, cIter in itertools.product(ranks, reguls, iters):
    
    modelEffectiveness = ALS.train(rddTrainingEffectiveness, cRank, cIter, float(cRegul))
    dist = howFarAreWe(modelEffectiveness, rddValidatingEffectiveness, nbValidatingEffectiveness)
    if dist < finalDistEffectiveness:
        print("Best so far:%f" % dist)
        finalModelEffectiveness = modelEffectiveness
        finalRankEffectiveness  = cRank
        finalRegulEffectiveness = cRegul
        finalIterEffectiveness  = cIter
        finalDistEffectiveness  = dist
#[END train_model_effectiveness]]

print() 
print("Effectiveness Rank %i" % finalRankEffectiveness) 
print("Effectiveness Iter %i" % finalIterEffectiveness)  
print("Effectiveness Regul %f" % finalRegulEffectiveness) 
print("Effectiveness Dist %f" % finalDistEffectiveness) 
















