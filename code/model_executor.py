#!/usr/bin/env python
"""
This code has been adapted from Google Cloud Platform's spark-recommendation-engine project that outlines a
recommendation model for Accommodations.
The code has been adapted and updated to execute two models based on Product data
The original source is available @ https://github.com/GoogleCloudPlatform/spark-recommendation-engine/tree/master/pyspark
"""
from __future__ import print_function

import sys
import itertools
from math import sqrt
from operator import add
from os.path import join, isfile, dirname
from pyspark import SparkContext, SparkConf, SQLContext
from pyspark.mllib.recommendation import ALS, MatrixFactorizationModel, Rating
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField
from pyspark.sql.types import StringType
from pyspark.sql.types import FloatType

#Test parameters
#CLOUDSQL_INSTANCE_IP = "35.196.146.149"
#CLOUDSQL_DB_NAME = "cognitive_recommendations"
#CLOUDSQL_USER = "appuser"
#CLOUDSQL_PWD  = "n0r00t4u"

CLOUDSQL_INSTANCE_IP = sys.argv[1]
CLOUDSQL_DB_NAME = sys.argv[2]
CLOUDSQL_USER = sys.argv[3]
CLOUDSQL_PWD  = sys.argv[4]

conf = SparkConf().setAppName("app_cognitiverecommendations")
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

#BEST_RANK = 20
#BEST_ITERATION = 10
#BEST_REGULATION = 0.1

BEST_RANK_FRAGRANCE = int(sys.argv[5])
BEST_ITERATION_FRAGRANCE = int(sys.argv[6])
BEST_REGULATION_FRAGRANCE = float(sys.argv[7])

BEST_RANK_EFFECTIVENESS = int(sys.argv[8])
BEST_ITERATION_EFFECTIVENESS = int(sys.argv[9])
BEST_REGULATION_EFFECTIVENESS = float(sys.argv[10])

USER_ID = float(sys.argv[11])

TABLE_ITEMS  = "Product"
TABLE_RATINGS_FRAGRANCE = "FragranceRating"
TABLE_RECOMMENDATIONS_FRAGRANCE = "FragranceRecommendation"
TABLE_RATINGS_EFFECTIVENESS = "EffectivenessRating"
TABLE_RECOMMENDATIONS_EFFECTIVENESS = "EffectivenessRecommendation"

# Read the data from the Cloud SQL
# Create dataframes
#[START read_from_sql]
jdbcUrl    = 'jdbc:mysql://%s:3306/%s?user=%s&password=%s&useSSL=false' % (CLOUDSQL_INSTANCE_IP, CLOUDSQL_DB_NAME, CLOUDSQL_USER, CLOUDSQL_PWD)
dfProduct = sqlContext.read.jdbc(url=jdbcUrl, table=TABLE_ITEMS)
dfRatingsFragrance = sqlContext.read.jdbc(url=jdbcUrl, table=TABLE_RATINGS_FRAGRANCE)
dfRatingsEffectiveness = sqlContext.read.jdbc(url=jdbcUrl, table=TABLE_RATINGS_EFFECTIVENESS)
#[END read_from_sql]

# Get all the fragrance ratings rows of our user
dfUserRatingsFragrance  = dfRatingsFragrance.filter(dfRatingsFragrance.userId == USER_ID).rdd.map(lambda r: r.productId).collect()
print(dfUserRatingsFragrance)

# Returns only the products that have not been rated by our user
rddPotential  = dfProduct.rdd.filter(lambda x: x[0] not in dfUserRatingsFragrance)
pairsPotential = rddPotential.map(lambda x: (USER_ID, x[0]))

#[START split_sets]
rddTraining, rddValidating, rddTesting = dfRatingsFragrance.rdd.randomSplit([6,2,2])
#[END split_sets]

#[START predict]
# Build our model with the best found values
# Rating, Rank, Iteration, Regulation
sc.setCheckpointDir('checkpoint/')
ALS.checkpointInterval = 2
model = ALS.train(rddTraining, BEST_RANK_FRAGRANCE, BEST_ITERATION_FRAGRANCE, BEST_REGULATION_FRAGRANCE)

# Calculate all predictions
predictions = model.predictAll(pairsPotential).map(lambda p: (str(p[0]), str(p[1]), float(p[2])))

# Take the top 5 ones
topPredictions = predictions.takeOrdered(5, key=lambda x: -x[2])
print(topPredictions)

schema = StructType([StructField("userId", StringType(), True), StructField("productId", StringType(), True), StructField("prediction", FloatType(), True)])

#[START save_top]
dfToSave = sqlContext.createDataFrame(topPredictions, schema)
dfToSave.write.jdbc(url=jdbcUrl, table=TABLE_RECOMMENDATIONS_FRAGRANCE, mode='overwrite')
#[END save_top]




# Get all the effectiveness ratings rows of our user
dfUserRatingsEffectiveness  = dfRatingsEffectiveness.filter(dfRatingsEffectiveness.userId == USER_ID).rdd.map(lambda r: r.productId).collect()
print(dfUserRatingsEffectiveness)

# Returns only the products that have not been rated by our user
rddPotential  = dfProduct.rdd.filter(lambda x: x[0] not in dfUserRatingsEffectiveness)
pairsPotential = rddPotential.map(lambda x: (USER_ID, x[0]))

#[START split_sets]
rddTraining, rddValidating, rddTesting = dfRatingsEffectiveness.rdd.randomSplit([6,2,2])
#[END split_sets]

#[START predict]
# Build our model with the best found values
# Rating, Rank, Iteration, Regulation
sc.setCheckpointDir('checkpoint/')
ALS.checkpointInterval = 2
model = ALS.train(rddTraining, BEST_RANK_EFFECTIVENESS, BEST_ITERATION_EFFECTIVENESS, BEST_REGULATION_EFFECTIVENESS)

# Calculate all predictions
predictions = model.predictAll(pairsPotential).map(lambda p: (str(p[0]), str(p[1]), float(p[2])))

# Take the top 5 ones
topPredictions = predictions.takeOrdered(5, key=lambda x: -x[2])
print(topPredictions)

schema = StructType([StructField("userId", StringType(), True), StructField("productId", StringType(), True), StructField("prediction", FloatType(), True)])

#[START save_top]
dfToSave = sqlContext.createDataFrame(topPredictions, schema)
dfToSave.write.jdbc(url=jdbcUrl, table=TABLE_RECOMMENDATIONS_EFFECTIVENESS, mode='overwrite')
#[END save_top]
